import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <div style={{ textAlign: 'center' }}>
      <p>Page Not Found</p>
      <Link to='/'>Clik Here to go to Home Page</Link>
    </div>
  )
};

export default NotFound;
