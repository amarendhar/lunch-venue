import React, { Fragment } from 'react';
import styled, { css } from 'styled-components';
import { get } from 'lodash';
import SelectVenue from '../SelectVenue';

const VenuewContainer = ({ venues, participants, onSelectVenue, onChangeName }) => {
  return (
    <VenuesContainer>
      <VenueBody>
        <Venues>
          {venues.map((item, index) => (
            <Fragment key={item.id}>
              {index === 0 && (
                <Cell>Participants</Cell>
              )}
              <Cell>
                <div>{item.name}</div>
                <div>{get(item, 'categories.0.name')}</div>
                <div>{get(item, 'rating', '--')}</div>
              </Cell>
            </Fragment>
          ))}
        </Venues>
        {venues.length > 0 ? participants.map((participant, key) => (
          <Venues key={participant.id}>
            <SelectVenue participant={participant} venues={venues} onSelectVenue={onSelectVenue} onChangeName={onChangeName} index={key} />
          </Venues>
        )) : ''}
      </VenueBody>
    </VenuesContainer>
  )
}

export default VenuewContainer;

const centerAlign = css`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const VenuesContainer = styled.table`
  ${centerAlign};
  border-collapse: collapse;
`;

const VenueBody = styled.tbody`
  border-radius: 10px;
  overflow: hidden;
  border: 1px solid #8888;
  box-shadow: 0 2px 15px 0 rgba(0, 0, 0, 0.2);
`;

const Venues = styled.tr`
  display: grid;
  grid-auto-columns: 200px;
  grid-auto-flow: column;
`;

const Cell = styled.td`
  ${centerAlign};
  flex-direction: column;
  text-align: center;
  padding: 10px;
  border: 1px solid #8888;
`;
