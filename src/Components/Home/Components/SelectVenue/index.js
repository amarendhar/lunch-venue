import React, { Fragment } from 'react';
import styled, { css } from 'styled-components';
import Done from '@material-ui/icons/Done';

const SelectVenue = ({ participant, venues, onChangeName, onSelectVenue, index }) => {
  return (
    <Fragment>
      <Cell>
        <Input type="text" placeholder="Type Name here" onChange={onChangeName(index)} value={participant.name} />
      </Cell>
      {venues.map(venue => (
        <Cell key={venue.id} onClick={onSelectVenue(index, venue.id)}>
          {participant.venuId === venue.id && (
            <Selected />
          )}
        </Cell>
      ))}
    </Fragment>
  )
}

export default SelectVenue;

const centerAlign = css`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Input = styled.input`
  width: 100%;
  height: 35px;
  padding: 8px;
`;

const Cell = styled.td`
  ${centerAlign};
  flex-direction: column;
  text-align: center;
  padding: 10px;
  border: 1px solid #8888;
  cursor: pointer;

  &:hover {
    background-color: aliceblue;
  }
`;

const Selected = styled(Done)`
  font-size: 35px !important;
`