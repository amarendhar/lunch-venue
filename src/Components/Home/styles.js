export default theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: '25px 0',
    textAlign: 'center',
  },
  loader: {
    flexGrow: 1,
    margin: '20px 0',
    textAlign: 'center',
  },
});
