import React, { useEffect, useReducer, useState } from 'react';
import styled, { css } from 'styled-components';
import axios from 'axios';
import { get } from 'lodash';
import { VENUE_URL, STAGES } from '../../utils/constants';
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import styles from './styles';
import uuidv4 from 'uuid/v4';
import VenuesContainer from './Components/VenuesContainer';

const initialState = {
  loading: true,
  venues: [],
  geocode: {},
  error: false,
}
const initialParticipant = id => [
  {
    id,
    name: '',
    venuId: '',
  }
];

const reducer = (state, action) => {
  const { type, payload } = action;

  switch(type) {
    case STAGES.PENDING:
      return { ...initialState };
    case STAGES.FULFILLED:
      return { ...payload, loading: false };
    case STAGES.REJECTED:
      return { ...initialState, loading: false, error: true };
    default:
      return state;
  }
}

const Home = ({ classes }) => {
  const [ place, setPlace ] = useState('10999 Berlin');
  const [ state, dispatch ] = useReducer(reducer, initialState);
  const [ participants, setParticipants ] = useState(initialParticipant(uuidv4()));

  const fetchLunchVenues = async (e) => {
    e && e.preventDefault();
    setParticipants(initialParticipant(uuidv4()));
    try {
      const { data: { response } } = await axios.get(VENUE_URL, {
        params: {
          query: 'lunch',
          near: place,
          v: 20180323,
          limit: 3
        }
      });

      dispatch({
        type: STAGES.FULFILLED,
        payload: response,
      });
    } catch(err) {
      dispatch({
        type: STAGES.REJECTED,
        message: get(err, 'response.data.message') || get(err, 'message') || JSON.stringify(err),
      });
      console.info('Error in fetchLunchVenues -> ', err);
    }
  }

  const action = () => (
    <Button color="secondary" size="small" onClick={fetchLunchVenues}>
      Click here to Reload/Re-fetch.
    </Button>
  );

  const onChangePlace = e => {
    setPlace(e.target.value);
  }

  const onAddParticipant = () => {
    setParticipants([
      ...participants,
      ...initialParticipant(uuidv4()),
    ])
  }

  const onChangeName = (index) => e => {
    participants[index] = {
      ...participants[index],
      name: e.target.value,
    }
    setParticipants([...participants]);
  }

  const onSelectVenue = (index, venuId) => () => {
    participants[index] = {
      ...participants[index],
      venuId,
    }
    setParticipants([...participants]);
  }

  useEffect(() => {
    fetchLunchVenues();
  }, []);

  return (
    <div>
      <Typography variant="h5" component="p" className={classes.title}>
        Lunch Place
      </Typography>
      {state.loading && (
        <div className={classes.loader} data-testid="forecast-loading">
          <LinearProgress />
          <br />
          <div>Loading...</div>
          <br />
          <LinearProgress color="secondary" />
        </div>
      )}
      {state.err && (
        <SnackbarContent message="Network Error. Please try later." action={action()} style={{ margin: '20px auto' }} />
      )}
      <SearchContainer onSubmit={fetchLunchVenues}>
        <Input type="text" placeholder={place} onChange={onChangePlace} value={place} />
        <Button variant="contained" color="primary" type="submit">
          Search
        </Button>
      </SearchContainer>
      {state.venues.length > 0 ? (
        <VenuesContainer venues={state.venues} participants={participants} onSelectVenue={onSelectVenue} onChangeName={onChangeName} />
      ): ''}
      <ButtonContainer>
        <Button variant="contained" color="primary" onClick={onAddParticipant}>
          Add Participant
        </Button>
      </ButtonContainer>
    </div>
  )
}

export default withStyles(styles)(Home);

const centerAlign = css`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ButtonContainer = styled.div`
  ${centerAlign};
  margin-top: 25px;
`;

const SearchContainer = styled.form`
  ${centerAlign};
  margin-bottom: 25px;
  button {
    margin-left: 25px;
  }
`;

const Input = styled.input`
  height: 35px;
  padding: 8px;
`;
