import React from 'react';
import styled from 'styled-components';

const Footer = () => {
  return (
    <Container>@Lunch Venue</Container>
  )
};

export default Footer;

const Container = styled.div`
  background-color: rgba(0, 0, 0, 0.08);
  padding: 20px;
  text-align: center;
`;
