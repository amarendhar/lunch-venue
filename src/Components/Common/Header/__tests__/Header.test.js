import React from 'react';
import renderWithRouter from '../../../../utils/renderWithRouter';
import Header from '../index';

describe('Header Component', () => {
  it('renders component', () => {
    const { container } = renderWithRouter(
        <Header />
    );

    expect(container).toMatchSnapshot();
  });
})