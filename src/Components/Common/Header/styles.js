export default theme => ({
  root: {
    width: '100%',
  },
  grow: {
    flexGrow: 1,
  },
  homeButton: {
    color: 'white',
  },
  homeButtonText: {
    [theme.breakpoints.down('sm')]: {
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
    },
  },
  menuButton: {
    marginLeft: -12,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
});
