import React, { Component } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import Link from '@material-ui/core/Link';
import {
  AppBar,
  Toolbar,
  Button,
  Menu,
  MenuItem,
  Drawer,
  List,
  Divider,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core';

import {
  IconMenu,
  IconHome,
  IconGitHub,
  IconLinkedIn,
  IconMore,
} from '../../Icons';

import { withStyles } from '@material-ui/core/styles';

import styles from './styles';

class Header extends Component {
  state = {
    mobileMoreAnchorEl: null,
    openSideMenu: false,
  };

  handleMobileMenuOpen = e => {
    this.setState({ mobileMoreAnchorEl: e.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  toggleDrawer = open => () => {
    this.setState({
      openSideMenu: open,
    });
  };

  render() {
    const { classes } = this.props;
    const { mobileMoreAnchorEl } = this.state;
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    return (
      <div className={classes.root}>
        <AppBar position="fixed" color="primary">
          <Toolbar variant="regular">
            <IconMenu
              className={classes.menuButton}
              aria-label="Open Menu"
              onClick={this.toggleDrawer(true)}
            />
            <Link
              to="/"
              component={RouterLink}
              underline="none"
              color="inherit"
            >
              <Button className={classes.homeButton} size="large">
                <span className={classes.homeButtonText}>Lunch Venue</span>
              </Button>
            </Link>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <a href="https://github.com/amarendhar" target="_blank" rel="noopener noreferrer">
                <IconGitHub
                  aria-label="GitHub Repository"
                  svgColor="white"
                  svgStyle={{ fontSize: 24 }}
                />
              </a>
              <a href="https://www.linkedin.com/in/amarendhar" target="_blank" rel="noopener noreferrer">
                <IconLinkedIn
                  aria-label="LinkedIn Profile"
                  svgColor="white"
                  svgStyle={{ fontSize: 24, padding: '1px' }}
                />
              </a>
            </div>
            <IconMore
              className={classes.sectionMobile}
              aria-haspopup="true"
              aria-label="Open Right Menu"
              onClick={this.handleMobileMenuOpen}
            />
          </Toolbar>
        </AppBar>

        {/* renderMobileMenu */}
        <Menu
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          anchorEl={mobileMoreAnchorEl}
          open={isMobileMenuOpen}
          onClose={this.handleMobileMenuClose}
        >
          <a href="https://github.com/amarendhar" target="_blank" rel="noopener noreferrer" style={{ outline: 'none', textDecoration: 'none'}}>
            <MenuItem onClick={this.handleMobileMenuClose}>
              <IconGitHub
                aria-label="GitHub Repository"
                svgColor="black"
                svgStyle={{ fontSize: 24 }}
              />
              <p>GitHub</p>
            </MenuItem>
          </a>
          <a href="https://www.linkedin.com/in/amarendhar" target="_blank" rel="noopener noreferrer" style={{ outline: 'none', textDecoration: 'none'}}>
            <MenuItem onClick={this.handleProfileMenuOpen}>
              <IconLinkedIn
                aria-label="LinkedIn Profile"
                svgColor="black"
                svgStyle={{ fontSize: 24, padding: '1px' }}
              />
              <p>LinkedIn</p>
            </MenuItem>
          </a>
        </Menu>

        {/* Drawer sideMenu */}
        <Drawer
          open={this.state.openSideMenu}
          onClose={this.toggleDrawer(false)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer(false)}
            onKeyDown={this.toggleDrawer(false)}
          >
            <div className={classes.list}>
              <List>
                <Link
                  to="/"
                  component={RouterLink}
                  underline="none"
                  color="inherit"
                >
                  <ListItem button>
                    <ListItemIcon>
                      <IconHome
                        aria-label="Lunch Venue"
                        htmlColor="black"
                        viewBox="1 1 22 22"
                        iconStyle={{ fontSize: 26 }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="Lunch Venue" />
                  </ListItem>
                </Link>
              </List>
              <Divider />
              <List>
                <a href="https://github.com/amarendhar" target="_blank" rel="noopener noreferrer" style={{ textDecoration: 'none'}}>
                  <ListItem button>
                    <ListItemIcon>
                        <IconGitHub
                          aria-label="GitHub Repository"
                          svgColor="black"
                          svgStyle={{ fontSize: 24 }}
                        />
                    </ListItemIcon>
                    <ListItemText primary="GitHub" />
                  </ListItem>
                </a>
                <a href="https://www.linkedin.com/in/amarendhar" target="_blank" rel="noopener noreferrer" style={{ textDecoration: 'none'}}>
                  <ListItem button>
                    <ListItemIcon>
                      <IconLinkedIn
                        aria-label="LinkedIn Profile"
                        svgColor="black"
                        svgStyle={{ fontSize: 24, padding: '1px' }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="LinkedIn" />
                  </ListItem>
                </a>
              </List>
            </div>
          </div>
        </Drawer>
      </div>
    );
  }
}

export default withStyles(styles)(Header);
