import styled from 'styled-components';

export default theme => ({
  offsetHeader: theme.mixins.toolbar,
  temp: {
    margin: 53,
  }
});

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  min-height: 100vh;
`;
