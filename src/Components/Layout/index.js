import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles'
import styles, { Wrapper } from './styles';
import Header from '../Common/Header';
import Footer from '../Common/Footer';

class Layout extends Component {
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Wrapper>
        <Header />
        <div className={classes.offsetHeader} />
        <div style={{ flex: 1 }}>
          {this.props.children}
        </div>
        <Footer />
      </Wrapper>
    )
  }
};

export default compose(
  withRouter,
  withStyles(styles),
)(Layout);
