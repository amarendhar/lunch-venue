// https://api.foursquare.com/v2/venues/explore?client_id=CLIENT_ID&client_secret=CLIENT_SECRET&v=20180323&limit=1&ll=40.7243,-74.0018&query=coffee

// https://api.foursquare.com/v2/venues/search?client_id=NUWYW5RBNTV120EA44NCNR2CQVPYFDOA0P5VRB5S5MY5G3ET&client_secret=NNB12PDXDYNWMS40WEU4RAUGOVTCMGX40KLAAB0NZIGXG51Q&query=lunch&near=10999%20Berlin&v=20180323&limit=3

export const CLIENT_ID = 'NUWYW5RBNTV120EA44NCNR2CQVPYFDOA0P5VRB5S5MY5G3ET';
export const CLIENT_SECRET = 'NNB12PDXDYNWMS40WEU4RAUGOVTCMGX40KLAAB0NZIGXG51Q';
export const ENDPOINT = 'https://api.foursquare.com/v2';

export const VENUE_URL = `${ENDPOINT}/venues/search?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&query=lunch&near=10999%20Berlin&v=20180323&limit=3`
// https://api.foursquare.com/v2/venues/explore?client_id=NUWYW5RBNTV120EA44NCNR2CQVPYFDOA0P5VRB5S5MY5G3ET&client_secret=NNB12PDXDYNWMS40WEU4RAUGOVTCMGX40KLAAB0NZIGXG51Q&query=lunch&ll=40.7243,-74.0018&v=20180323&limit=3
// https://api.foursquare.com/v2/venues/search?client_id=NUWYW5RBNTV120EA44NCNR2CQVPYFDOA0P5VRB5S5MY5G3ET&client_secret=NNB12PDXDYNWMS40WEU4RAUGOVTCMGX40KLAAB0NZIGXG51Q&query=lunch&near=Chicago,%20IL&v=20180323&limit=3

export const STAGES = {
  PENDING: 'PENDING',
  FULFILLED: 'FULFILLED',
  REJECTED: 'REJECTED'
};
