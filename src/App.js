import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import Layout from './Components/Layout';
import Home from './Components/Home';
import NotFound from './Components/NotFound';

const App = () => {
  return (
    <>
      <CssBaseline />
      <Router>
        <Layout>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/notfound' component={NotFound} />
            <Redirect from='*' to="/notfound" />
          </Switch>
        </Layout>
      </Router>
    </>
  )
};

export default App;
